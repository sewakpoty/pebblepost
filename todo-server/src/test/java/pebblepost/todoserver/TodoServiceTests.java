package pebblepost.todoserver;

import javassist.NotFoundException;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import pebblepost.todoserver.model.Todo;
import pebblepost.todoserver.repository.TodoRepository;
import pebblepost.todoserver.service.TodoService;
import pebblepost.todoserver.service.TodoServiceImpl;

import javax.swing.text.html.parser.Entity;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@SpringBootTest
public class TodoServiceTests {

    private final Long ID = 1L;
    private final String LABEL = "TODO";
    private final String NEW_LABEL = "TODO1";

    TodoService todoService;

    @Mock
    TodoRepository todoRepository;

    @BeforeEach
    public void setUp() throws Exception{
        MockitoAnnotations.openMocks(this);
        todoService = new TodoServiceImpl(todoRepository);
    }

    @Test
    public void createTodo(){
        Todo todo = new Todo();
        todo.setId(ID);
        todo.setLabel(LABEL);

        when(todoRepository.save(todo)).thenReturn(todo);

        Todo todo1 = todoService.createTodo(todo);
        assertEquals("",todo.getId(),todo1.getId());
    }

    @Test
    public void getTodos(){

        List<Todo> todos = Arrays.asList(new Todo(), new Todo(), new Todo());
        when(todoRepository.findAll()).thenReturn(todos);

        List<Todo> todos1 = todoService.getTodos();

        assertEquals("Null Found",3, todos1.size());
    }

    @Test
    public void getTodo() throws NotFoundException {
        Todo todo = new Todo();
        todo.setId(ID);
        todo.setLabel(LABEL);

        when(todoRepository.getOne(ID)).thenReturn(todo);

        Todo todo1 = todoService.getTodo(ID);
        assertEquals("", LABEL, todo1.getLabel());
    }

    @Test
    public void updateTodo(){
        Todo todo = new Todo();
        todo.setId(ID);
        todo.setLabel(LABEL);

        when(todoRepository.save(todo)).thenReturn(todo);

        Todo todo1 = todoService.createTodo(todo);
        assertEquals("",todo.getId(),todo1.getId());

        todo1.setLabel(NEW_LABEL);
        when(todoRepository.save(todo1)).thenReturn(todo1);
        Todo todo2 = todoService.updateTodo(todo1.getId(),todo1);

        assertEquals("",todo2.getId(),todo1.getId());
        assertEquals("",todo1.getLabel(),todo2.getLabel());
    }

    @Test
    public void deleteTodo() throws NotFoundException {

        Todo todo = new Todo();
        todo.setId(ID);
        todo.setLabel(LABEL);

        doNothing().doThrow( new IllegalStateException() )
                .when( todoRepository ).deleteById( todo.getId() );

        when(todoRepository.getOne(ID)).thenReturn(todo);

        Todo todo1 = todoService.getTodo(ID);

        todoService.deleteTodo(todo1.getId());

        verify(todoRepository,atLeast(1)).deleteById(todo1.getId());
    }

}
