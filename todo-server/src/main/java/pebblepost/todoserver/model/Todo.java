package pebblepost.todoserver.model;


import pebblepost.todoserver.dto.TodoDto;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

@Entity
public class Todo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String label;

    private boolean completed;

    public Todo() {}

    public Todo(Long id, String label, boolean completed) {
        this.id = id;
        this.label = label;
        this.completed = completed;
    }

    public Todo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        completed = completed;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        public Builder id() {
            return this;
        }

        public Todo build(TodoDto dto) {
            return new Todo(dto.getId(),dto.getLabel(),dto.isCompleted());
        }

    }
}
