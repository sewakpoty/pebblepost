package pebblepost.todoserver.service;

import javassist.NotFoundException;
import pebblepost.todoserver.model.Todo;

import java.util.List;

public interface TodoService {

    Todo createTodo(Todo newTodo);

    List<Todo> getTodos();

    Todo getTodo(Long id) throws NotFoundException;

    Todo updateTodo(Long id, Todo updatedTodo);

    void deleteTodo(Long id);
}
