package pebblepost.todoserver.service;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import pebblepost.todoserver.model.Todo;
import pebblepost.todoserver.repository.TodoRepository;

import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {

    private final TodoRepository todoRepository;

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public Todo createTodo(Todo newTodo) {
        return todoRepository.save(newTodo) ;
    }

    @Override
    public List<Todo> getTodos() {
        return todoRepository.findAll();
    }

    @Override
    public Todo getTodo(Long id) throws NotFoundException {
        return todoRepository.getOne(id);
    }

    @Override
    public Todo updateTodo(Long id, Todo updatedTodo) {
        updatedTodo.setId(id);
        return createTodo(updatedTodo);
    }

    @Override
    public void deleteTodo(Long id) {
        todoRepository.deleteById(id);
    }
}
