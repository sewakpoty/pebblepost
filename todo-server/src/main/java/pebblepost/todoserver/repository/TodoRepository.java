package pebblepost.todoserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pebblepost.todoserver.model.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
