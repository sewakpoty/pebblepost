package pebblepost.todoserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import pebblepost.todoserver.model.Todo;

public class TodoDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    private String label;
    private boolean completed;

    public TodoDto() {
    }

    public TodoDto(Long id, String label, boolean completed) {
        this.id = id;
        this.label = label;
        this.completed = completed;
    }

    public static TodoDto fromEntity(Todo todo) {
        return TodoDto.builder().build(todo);
    }

    public static Todo toEntity(TodoDto dto) {
        return Todo.builder().build(dto);
    }

    private static Builder builder() {
        return new Builder();
    }

    private static class Builder {

        public TodoDto build(Todo todo) {
            return new TodoDto(todo.getId(),todo.getLabel(),todo.isCompleted());
        }

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
