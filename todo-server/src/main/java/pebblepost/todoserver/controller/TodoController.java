package pebblepost.todoserver.controller;


import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pebblepost.todoserver.dto.TodoDto;
import pebblepost.todoserver.model.Todo;
import pebblepost.todoserver.service.TodoService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/todos")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDto create(@RequestBody TodoDto createDto) {
        Todo newTodo = todoService.createTodo(TodoDto.toEntity(createDto));
        return TodoDto.fromEntity(newTodo);
    }

    @GetMapping
    public List<TodoDto> getAll() {
        return todoService.getTodos().stream()
                .map(todo-> TodoDto.fromEntity(todo))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public TodoDto getOne(@PathVariable("id") Long id) {
        try {
            return TodoDto.fromEntity(todoService.getTodo(id));
        }catch (NotFoundException ex){
            ex.printStackTrace();
        }
        return null;
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public TodoDto put(@PathVariable("id") Long id, @RequestBody TodoDto updated) {
        Todo updatedTodo = todoService.updateTodo(id,TodoDto.toEntity(updated));
        return TodoDto.fromEntity(updatedTodo);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        todoService.deleteTodo(id);
    }
}
