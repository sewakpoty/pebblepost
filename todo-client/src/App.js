import './App.css';
import TodoListPage from './page/TodoListPage';

const App = () => (
    <div className="App">
      <TodoListPage/>
    </div>
)

export default App;
