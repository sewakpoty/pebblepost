import React,{useEffect, useState} from "react";
import BottomMenu from "../component/BottomMenu/BottomMenu";
import TodoForm from "../component/TodoForm/TofoForm";
import TodoList from "../component/TodoList/TodoList";
import "./TodoListPage.css";


const TodoListPage = () => {


    const server = "http://localhost:8080/todos";
    const [todoList,setTodoList] = useState([]);
    const [refetchALL, setRefetchALL] = useState(false);
    const [counters, setCounters] = useState({type:"All", All:0, Completed:0,Active:0});
    const [todo,setTodo] = useState({});

    const fetchAllTodos = () =>{
        fetch(server)
        .then(response => response.json())
        .then(todoList => {setTodoList(todoList);})
    }

    useEffect(()=>{
        updateCounters();
    },[todoList])

    const updateCounters = () => {
        let all = todoList.length;
        let completed = 0; 
        let active = 0;
        todoList.map(todo => {
            (todo.completed)?completed++:active++;
        })
        setCounters({...counters, All:all, Completed:completed, Active:active});
    }

    const updateTodo = (id,todo) => {
        fetch(server+'/'+id, {method: 'PUT', body: JSON.stringify(todo),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }}).then(response => response.json())
                .then(result => {
                    console.log('Success:', result);
                    setRefetchALL(!refetchALL);
                }).catch(error => {
                    console.error('Error:', error);
                }).then(setTodo({}));
    }

    const addTodo = () => {
        fetch(server,{ method: 'POST',body: JSON.stringify(todo),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
         }).then(response => response.json())
            .then(result => {
                console.log('Success:', result);
                setRefetchALL(!refetchALL);
         }).then(setTodo({}));
    }

    const deleteTodo = (id) => {
        console.log(id);
        fetch(server+'/'+id, {method: 'DELETE'})
            .then(result => {
                console.log('Success:', result);
                setRefetchALL(!refetchALL);
            })
    }

    useEffect(()=>{ fetchAllTodos() },[]);

    useEffect(()=>{fetchAllTodos()},[refetchALL]);


    const handleSubmit = () => {
        if(!todo || !todo.label) {
            return;
        }
        if(todo.id) updateTodo(todo.id,todo);
        else addTodo();
    }

    const handleCompleted = (id) =>{
        let completedTodo = todoList.find(todo => todo.id===id);
        completedTodo.completed = !completedTodo.completed;
        updateTodo(id,completedTodo);
    }

    const handleDelete = (id) =>{ deleteTodo(id)}

    const handleCounter = (type) => {
        switch (type) {
            case 'All':
                setCounters({...counters,type:"All"});
                break;
            case 'Completed':
                setCounters({...counters,type:"Completed"});
                break;
            case 'Active':
                setCounters({...counters,type:"Active"});
                break;
        }
    }

    const handleUpdateClick = (id) =>{
        setTodo(todoList.find(todo => todo.id===id));
    }

    const handleChange = (e) => {
        setTodo({...todo,label:e.target.value});
    }

    const handleClearAll = (e) => {
        todoList.map(todo => {
            if(todo.completed){
                todo.completed = false;
                updateTodo(todo.id,todo);
            }
        })
    }

    return(
        <div id="TodoListPage">
            <TodoForm handleSubmit={handleSubmit} handleChange={handleChange} todo={todo} />
            <TodoList todoList={todoList} handleCompleted={handleCompleted} handleDelete={handleDelete} handleUpdateClick={handleUpdateClick}/>
            <BottomMenu counters={counters} handleCounter={handleCounter} handleClearAll={handleClearAll} />
        </div>   
    );
}
export default TodoListPage;