import React from "react";
import "./BottomMenu.css";

const BottomMenu = ({counters,handleCounter,handleClearAll}) => {
    
    return (

        <div id="BottomMenu">
            <div className="counter">{counters[counters.type]} item(s)</div>
            <div className={`All ${counters.type==="All"?'currentCounter':''}`} onClick={e => handleCounter("All")}>All</div>
            <div className={`Active ${counters.type==="Active"?'currentCounter':''}`}  onClick={e => handleCounter("Active")}>Active</div>
            <div className={`Completed ${counters.type==="Completed"?'currentCounter':''}`} onClick={e => handleCounter("Completed")}>Completed</div>
            <div className="clearCompleted" onClick={handleClearAll}>Clear Completed</div>
        </div>

    )
}

export default BottomMenu;
