import React from "react";
import Todo from "../Todo/Todo";
import "./TodoList.css";


const TodoList = ({todoList,...otherProps}) => {
    return(
    
        <div id="TodoList">
            {
                (todoList.length>0)?
                todoList.map(todo => <Todo key={todo.id} todo={todo} {...otherProps}/>)
                : <h2> NO TODOS TO DISPLAY </h2>
            }
        </div> 
        
    )

}

export default TodoList;