import React,{useState} from "react";
import "./Todo.css";
import deleteImg from "../../image/delete.png";
import editImg from "../../image/edit.png";


const Todo = ({todo,handleDelete,handleUpdateClick,handleCompleted}) => {

    const [isUpdateActive, setUpDateActive ] = useState(false);
     
    return(
        <div className='todo' onMouseOver={e=>setUpDateActive(true)} onMouseOut={e=>setUpDateActive(false)}>
            <div className="check">
                <input checked={todo.completed} onChange={e=>handleCompleted(todo.id)} type="checkbox"/>
            </div>
            <div className="label">
                {todo.label}
            </div>  
            <div className={`edit ${isUpdateActive?'show':'hide'}`} onClick={e=>handleUpdateClick(todo.id)}>
                <img src={editImg} width={30} alt="editImg" />
            </div>
            <div className={`delete ${isUpdateActive?'show':'hide'}`} onClick={e=>handleDelete(todo.id)}>
                <img src={deleteImg}  width={30} alt="Delete" />
            </div>  
        </div>
    );
}

export default Todo;