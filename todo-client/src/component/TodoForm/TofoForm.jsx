import React,{useState} from "react"
import "./TodoForm.css"

const TodoForm = ({todo, handleChange, handleSubmit}) => {

    const label = (todo&&todo.label)?todo.label:'';

    return(
        <div id="todoInput">
            <input type="text" autoComplete="off" value={label} onChange={handleChange}  name="todoLabel" placeholder="Enter a todo" className="todoLabel"/>
            <button className="todoButton" onClick={handleSubmit}>Save</button>
        </div>
    )
}

export default TodoForm;